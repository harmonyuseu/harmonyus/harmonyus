package eu.harmonyus.gateway.route;

import eu.harmonyus.gateway.client.model.Category;
import eu.harmonyus.gateway.client.service.CategoryService;
import eu.harmonyus.gateway.infrastructure.ApplicationConfiguration;
import io.quarkus.vertx.web.Param;
import io.quarkus.vertx.web.Route;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpMethod;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class CategoryRoute {

    @Inject
    ApplicationConfiguration applicationConfiguration;

    @Inject
    @RestClient
    CategoryService categoryService;

    @Route(path = "/categories", methods = HttpMethod.GET)
    Uni<List<Category>> getAll() {
        return categoryService.getAll()
                .ifNoItem().after(applicationConfiguration.getRoute().getCategory().getGetAll().getTimeOut()).fail()
                .onFailure().retry().atMost(applicationConfiguration.getRoute().getCategory().getGetAll().getMaxRetries());
    }

    @Route(path = "/categories/:code", methods = HttpMethod.GET)
    Uni<Category> getByCode(@Param("code") String code) {
        return categoryService.getByCode(code)
                .ifNoItem().after(applicationConfiguration.getRoute().getCategory().getGetByCode().getTimeOut()).fail()
                .onFailure().retry().atMost(applicationConfiguration.getRoute().getCategory().getGetByCode().getMaxRetries());
    }
}
