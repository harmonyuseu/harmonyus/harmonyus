package eu.harmonyus.gateway.route;

import eu.harmonyus.gateway.client.model.Post;
import eu.harmonyus.gateway.client.service.PostService;
import eu.harmonyus.gateway.infrastructure.ApplicationConfiguration;
import eu.harmonyus.gateway.route.mapper.PostMapper;
import io.quarkus.vertx.web.Param;
import io.quarkus.vertx.web.Route;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class PostRoute {

    @Inject
    ApplicationConfiguration applicationConfiguration;

    @Inject
    @RestClient
    PostService postService;

    @Route(path = "/posts", methods = HttpMethod.GET)
    Uni<List<Post>> getAll(RoutingContext rc, @Param Optional<String> authorCode, @Param Optional<String> categoryCode, @Param Optional<String> tagCode) {
        String url = rc.request().scheme() + "://" +  rc.request().host();
        return postService.getAll(authorCode.orElse(""), categoryCode.orElse(""), tagCode.orElse(""))
                .map(posts -> PostMapper.fromClientList(posts, url))
                .ifNoItem().after(applicationConfiguration.getRoute().getPost().getGetAll().getTimeOut()).fail()
                .onFailure().retry().atMost(applicationConfiguration.getRoute().getPost().getGetAll().getMaxRetries());
    }

    @Route(path = "/posts/:code", methods = HttpMethod.GET)
    Uni<Post> getByCode(RoutingContext rc, @Param("code") String code) {
        String url = rc.request().scheme() + "://" +  rc.request().host();
        return postService.getByCode(code)
                .map(post -> PostMapper.fromClient(post, url))
                .ifNoItem().after(applicationConfiguration.getRoute().getPost().getGetByCode().getTimeOut()).fail()
                .onFailure().retry().atMost(applicationConfiguration.getRoute().getPost().getGetByCode().getMaxRetries());
    }
}
