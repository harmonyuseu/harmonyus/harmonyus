package eu.harmonyus.gateway.route;

import eu.harmonyus.gateway.client.model.ApplicationParameters;
import eu.harmonyus.gateway.client.service.ApplicationParametersService;
import eu.harmonyus.gateway.infrastructure.ApplicationConfiguration;
import io.quarkus.vertx.web.Route;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpMethod;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class CoreRoute {

    @Inject
    ApplicationConfiguration applicationConfiguration;

    @Inject
    @RestClient
    ApplicationParametersService applicationParametersService;

    @Route(path = "/application-parameters", methods = HttpMethod.GET)
    public Uni<ApplicationParameters> getSystemInformation() {
        return applicationParametersService.getSystemInformation()
                .ifNoItem().after(applicationConfiguration.getRoute().getCore().getGetSystemInformation().getTimeOut()).fail()
                .onFailure().retry().atMost(applicationConfiguration.getRoute().getCore().getGetSystemInformation().getMaxRetries());
    }
}
