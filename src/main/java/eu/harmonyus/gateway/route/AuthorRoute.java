package eu.harmonyus.gateway.route;

import eu.harmonyus.gateway.client.model.Author;
import eu.harmonyus.gateway.client.service.AuthorService;
import eu.harmonyus.gateway.infrastructure.ApplicationConfiguration;
import io.quarkus.vertx.web.Param;
import io.quarkus.vertx.web.Route;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpMethod;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class AuthorRoute {

    @Inject
    ApplicationConfiguration applicationConfiguration;

    @Inject
    @RestClient
    AuthorService authorService;

    @Route(path = "/authors", methods = HttpMethod.GET)
    Uni<List<Author>> getAll() {
        return authorService.getAll()
                .ifNoItem().after(applicationConfiguration.getRoute().getAuthor().getGetAll().getTimeOut()).fail()
                .onFailure().retry().atMost(applicationConfiguration.getRoute().getAuthor().getGetAll().getMaxRetries());
    }

    @Route(path = "/authors/:code", methods = HttpMethod.GET)
    Uni<Author> getByCode(@Param("code") String code) {
        return authorService.getByCode(code)
                .ifNoItem().after(applicationConfiguration.getRoute().getAuthor().getGetByCode().getTimeOut()).fail()
                .onFailure().retry().atMost(applicationConfiguration.getRoute().getAuthor().getGetByCode().getMaxRetries());
    }
}
