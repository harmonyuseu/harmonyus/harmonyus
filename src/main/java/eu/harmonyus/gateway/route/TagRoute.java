package eu.harmonyus.gateway.route;

import eu.harmonyus.gateway.client.model.Tag;
import eu.harmonyus.gateway.client.service.TagService;
import eu.harmonyus.gateway.infrastructure.ApplicationConfiguration;
import io.quarkus.vertx.web.Param;
import io.quarkus.vertx.web.Route;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpMethod;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class TagRoute {

    @Inject
    ApplicationConfiguration applicationConfiguration;

    @Inject
    @RestClient
    TagService tagService;

    @Route(path = "/tags", methods = HttpMethod.GET)
    Uni<List<Tag>> getAll() {
        return tagService.getAll()
                .ifNoItem().after(applicationConfiguration.getRoute().getTag().getGetAll().getTimeOut()).fail()
                .onFailure().retry().atMost(applicationConfiguration.getRoute().getTag().getGetAll().getMaxRetries());
    }

    @Route(path = "/tags/:code", methods = HttpMethod.GET)
    Uni<Tag> getByCode(@Param("code") String code) {
        return tagService.getByCode(code)
                .ifNoItem().after(applicationConfiguration.getRoute().getTag().getGetByCode().getTimeOut()).fail()
                .onFailure().retry().atMost(applicationConfiguration.getRoute().getTag().getGetByCode().getMaxRetries());
    }
}
