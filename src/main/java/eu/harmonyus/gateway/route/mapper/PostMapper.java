package eu.harmonyus.gateway.route.mapper;

import eu.harmonyus.gateway.client.model.Post;

import java.util.ArrayList;
import java.util.List;

public final class PostMapper {

    private PostMapper() {

    }

    public static Post fromClient(Post postFromClient, String url) {
        Post post = new Post();
        post.title = postFromClient.title;
        post.body = postFromClient.body;
        post.code = postFromClient.code;
        post.creation = postFromClient.creation;
        post.modification = postFromClient.modification;
        post.author = new Post.UrlCode(url + "/authors/" + postFromClient.author.getCode());
        post.category = new Post.UrlCode(url + "/categories/" + postFromClient.category.getCode());
        post.tags = new ArrayList<>();
        postFromClient.tags.forEach(tag -> post.tags.add(new Post.UrlCode(url + "/tags/" + tag.getCode())));
        return post;
    }

    public static List<Post> fromClientList(List<Post> postsFromClient, String url) {
        List<Post> posts = new ArrayList<>();
        postsFromClient.forEach(post -> posts.add(fromClient(post, url)));
        return posts;
    }
}
