package eu.harmonyus.gateway.client.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.quarkus.runtime.annotations.RegisterForReflection;

import java.util.ArrayList;
import java.util.List;

@RegisterForReflection
public class Post {
    public String title;
    public String body;
    public String code;
    public String creation;
    public String modification;
    public UrlCode author;
    public UrlCode category;
    public List<UrlCode> tags;

    public Post() {
        this.author = new UrlCode();
        this.category = new UrlCode();
        this.tags = new ArrayList<>();
    }

    public static class UrlCode {
        public String url;

        public UrlCode() {

        }

        public UrlCode(String url) {
            this.url = url;
        }

        @JsonIgnore
        public String getCode() {
            return url.substring(url.lastIndexOf('/') + 1);
        }
    }
}
