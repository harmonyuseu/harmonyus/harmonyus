package eu.harmonyus.gateway.client.model;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class Tag {
    public String name;
    public String code;
}
