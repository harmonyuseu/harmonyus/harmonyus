package eu.harmonyus.gateway.client.service;

import eu.harmonyus.gateway.client.model.Post;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/posts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey="harmonyus.microservice.post")
public interface PostService {

    @GET
    Uni<List<Post>> getAll(@DefaultValue("") @QueryParam("authorCode") String authorCode,
                           @DefaultValue("") @QueryParam("categoryCode") String categoryCode,
                           @DefaultValue("") @QueryParam("tagCode") String tagCode);

    @GET
    @Path("/{code}")
    Uni<Post> getByCode(@PathParam("code") String code);

    @POST
    Uni<Response> create(Post post);

    @PUT
    @Path("/{code}")
    Uni<Post> modify(@PathParam("code") String code, Post post);

    @DELETE
    @Path("/{code}")
    Uni<Response> delete(@PathParam("code") String code);
}
