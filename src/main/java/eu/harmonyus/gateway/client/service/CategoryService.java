package eu.harmonyus.gateway.client.service;

import eu.harmonyus.gateway.client.model.Category;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/categories")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey="harmonyus.microservice.category")
public interface CategoryService {

    @GET
    Uni<List<Category>> getAll();

    @GET
    @Path("/{code}")
    Uni<Category> getByCode(@PathParam("code") String code);

    @POST
    Uni<Response> create(Category category);

    @PUT
    @Path("/{code}")
    Uni<Category> modify(@PathParam("code") String code, Category category);

    @DELETE
    @Path("/{code}")
    Uni<Response> delete(@PathParam("code") String code);
}
