package eu.harmonyus.gateway.infrastructure;

import io.quarkus.arc.config.ConfigProperties;

import java.time.Duration;
import java.util.Optional;

@ConfigProperties(prefix = "harmonyus")
public class ApplicationConfiguration {
    private Route route;

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
        route.author.manageGlobalSetting(route.global);
        route.category.manageGlobalSetting(route.global);
        route.core.manageGlobalSetting(route.global);
        route.post.manageGlobalSetting(route.global);
        route.tag.manageGlobalSetting(route.global);
    }

    public static class Route {
        private CRUDRoute author;
        private CRUDRoute category;
        private CoreRoute core;
        private CRUDRoute post;
        private CRUDRoute tag;
        private Fault global;


        public CRUDRoute getAuthor() {
            return author;
        }

        public void setAuthor(CRUDRoute author) {
            this.author = author;
        }

        public CRUDRoute getCategory() {
            return category;
        }

        public void setCategory(CRUDRoute category) {
            this.category = category;
        }

        public CoreRoute getCore() {
            return core;
        }

        public void setCore(CoreRoute core) {
            this.core = core;
        }

        public CRUDRoute getPost() {
            return post;
        }

        public void setPost(CRUDRoute post) {
            this.post = post;
        }

        public CRUDRoute getTag() {
            return tag;
        }

        public void setTag(CRUDRoute tag) {
            this.tag = tag;
        }

        public void setGlobal(Fault global) {
            this.global = global;
        }
    }

    public static class CRUDRoute {
        private Fault getAll;
        private Fault getByCode;
        private Fault global;

        public Fault getGetAll() {
            return getAll;
        }

        public void setGetAll(Fault getAll) {
            this.getAll = getAll;
        }

        public Fault getGetByCode() {
            return getByCode;
        }

        public void setGetByCode(Fault getByCode) {
            this.getByCode = getByCode;
        }

        public void setGlobal(Fault global) {
            this.global = global;
        }

        public void manageGlobalSetting(Fault rootGlobal) {
            getAll.timeOut = Optional.of(rootGlobal.timeOut.orElse(global.timeOut.orElse(getAll.timeOut.orElse(Duration.ofMillis(250)))));
            getAll.maxRetries = Optional.of(rootGlobal.maxRetries.orElse(global.maxRetries.orElse(getAll.maxRetries.orElse(4L))) + 1);
            getByCode.timeOut = Optional.of(rootGlobal.timeOut.orElse(global.timeOut.orElse(getByCode.timeOut.orElse(Duration.ofMillis(250)))));
            getByCode.maxRetries = Optional.of(rootGlobal.maxRetries.orElse(global.maxRetries.orElse(getByCode.maxRetries.orElse(4L))) + 1);
        }
    }

    public static class CoreRoute {
        private Fault getSystemInformation;
        private Fault global;

        public Fault getGetSystemInformation() {
            return getSystemInformation;
        }

        public void setGetSystemInformation(Fault getSystemInformation) {
            this.getSystemInformation = getSystemInformation;
        }

        public void setGlobal(Fault global) {
            this.global = global;
        }

        public void manageGlobalSetting(Fault rootGlobal) {
            getSystemInformation.timeOut = Optional.of(rootGlobal.timeOut.orElse(global.timeOut.orElse(getSystemInformation.timeOut.orElse(Duration.ofMillis(250)))));
            getSystemInformation.maxRetries = Optional.of(rootGlobal.maxRetries.orElse(global.maxRetries.orElse(getSystemInformation.maxRetries.orElse(4L))) + 1);
        }
    }

    public static class Fault {
        private Optional<Duration> timeOut;
        private Optional<Long> maxRetries;

        public Duration getTimeOut() {
            return timeOut.orElse(Duration.ofMillis(250));
        }

        public void setTimeOut(Optional<Duration> timeOut) {
            this.timeOut = timeOut;
        }

        public Long getMaxRetries() {
            return maxRetries.orElse(4L);
        }

        public void setMaxRetries(Optional<Long> maxRetries) {
            this.maxRetries = maxRetries;
        }
    }
}
