package eu.harmonyus.gateway.client.resource;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import eu.harmonyus.gateway.client.model.Post;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WiremockPostService implements QuarkusTestResourceLifecycleManager {

    private static WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        List<Post> testCase = getTestCase();
        wireMockServer = new WireMockServer(options().dynamicPort());
        wireMockServer.start();
        WireMock wireMock = new WireMock(wireMockServer.port());
        addMockGetPosts(wireMock);
        addMockGetPost(wireMock, "/posts/post-number-one", getPost(testCase.get(0), "post-number-one"));
        return Collections.singletonMap("harmonyus.microservice.post/mp-rest/url", wireMockServer.baseUrl());
    }

    public static void enableFaultFeature() {
        wireMockServer.setGlobalFixedDelay(500);
    }

    public static void disableFaultFeature() {
        wireMockServer.setGlobalFixedDelay(0);
    }

    private void addMockGetPosts(WireMock wireMock) {
        JSONArray objPosts = new JSONArray();
        List<Post> testCase = getTestCase();
        objPosts.appendElement(testCase.get(2));
        objPosts.appendElement(testCase.get(1));
        objPosts.appendElement(testCase.get(0));

        wireMock.register(get(urlEqualTo("/posts?authorCode=&categoryCode=&tagCode="))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(objPosts.toString())));
    }

    private void addMockGetPost(WireMock wireMock, String url, JSONObject objPost) {
        wireMock.register(get(urlEqualTo(url))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(objPost.toString())));
    }

    private void addMockPost(WireMock wireMock, JSONObject post) {
        wireMock.register(post(urlEqualTo("/posts"))
                .withRequestBody(equalToJson(post.toString()))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")));
    }

    public static JSONObject getPost(Post post, String code) {
        JSONObject obj = new JSONObject();
        obj.put("title", post.title);
        obj.put("code", code);
        obj.put("body", post.body);
        obj.put("author", post.author);
        obj.put("category", post.category);
        obj.put("tags", post.tags);
        return obj;
    }

    public static List<Post> getTestCase() {
        Post postOne = createPost("Post number one", "post-number-one");
        Post postTwo = createPost("Post number two", "post-number-two");
        Post postThree = createPost("Post number three", "post-number-three");
        return Arrays.asList(postOne, postTwo, postThree);
    }

    private static Post createPost(String title, String code) {
        Post post = new Post();
        post.title = title;
        post.code = code;
        post.body = "Body of the this post";
        post.author = new Post.UrlCode("https://author.domain.tld/john-mathew");
        post.category = new Post.UrlCode("http://category.domain.tld/category-one");
        post.tags = Arrays.asList(
                new Post.UrlCode("https://tag.domain.tld/tag-one"),
                new Post.UrlCode("https://tag.domain.tld/tag-two"),
                new Post.UrlCode("https://tag.domain.tld/tag-three"));
        post.creation = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        return post;
    }

    @Override
    public void stop() {
        if (null != wireMockServer) {
            wireMockServer.stop();
        }
    }
}
