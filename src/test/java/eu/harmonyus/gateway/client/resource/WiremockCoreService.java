package eu.harmonyus.gateway.client.resource;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;

import java.util.Collections;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WiremockCoreService  implements QuarkusTestResourceLifecycleManager {

    private static WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        wireMockServer = new WireMockServer(options().dynamicPort());
        wireMockServer.start();
        WireMock wireMock = new WireMock(wireMockServer.port());
        addMockGetApplicationParameters(wireMock, "/application-parameters");
        return Collections.singletonMap("harmonyus.microservice.core/mp-rest/url", wireMockServer.baseUrl());
    }

    public static void enableFaultFeature() {
        wireMockServer.setGlobalFixedDelay(500);
    }

    public static void disableFaultFeature() {
        wireMockServer.setGlobalFixedDelay(0);
    }

    private void addMockGetApplicationParameters(WireMock wireMock, String url) {
        wireMock.register(get(urlEqualTo(url))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"title\":\"harmonyus\", \"version\":\"0.1.0\"}")));
    }

    @Override
    public void stop() {
        if (null != wireMockServer) {
            wireMockServer.stop();
        }
    }
}
