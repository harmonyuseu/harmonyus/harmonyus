package eu.harmonyus.gateway.client.resource;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import eu.harmonyus.gateway.client.model.Category;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WiremockCategoryService implements QuarkusTestResourceLifecycleManager {

    private static WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        List<Category> testCase = getTestCase();
        wireMockServer = new WireMockServer(options().dynamicPort());
        wireMockServer.start();
        WireMock wireMock = new WireMock(wireMockServer.port());
        addMockGetCategories(wireMock);
        addMockGetCategory(wireMock, "/categories/category-one", getCategory(testCase.get(0), "category-one"));
        return Collections.singletonMap("harmonyus.microservice.category/mp-rest/url", wireMockServer.baseUrl());
    }

    public static void enableFaultFeature() {
        wireMockServer.setGlobalFixedDelay(500);
    }

    public static void disableFaultFeature() {
        wireMockServer.setGlobalFixedDelay(0);
    }

    private void addMockGetCategory(WireMock wireMock, String url, JSONObject objCategory) {
        wireMock.register(get(urlEqualTo(url))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(objCategory.toString())));
    }

    private void addMockGetCategories(WireMock wireMock) {
        JSONArray objCategories = new JSONArray();
        List<Category> testCase = getTestCase();
        objCategories.appendElement(testCase.get(0));
        objCategories.appendElement(testCase.get(1));
        objCategories.appendElement(testCase.get(2));

        wireMock.register(get(urlEqualTo("/categories"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(objCategories.toString())));
    }

    public static JSONObject getCategory(Category category, String code) {
        JSONObject obj = new JSONObject();
        obj.put("name", category.name);
        obj.put("code", code);
        return obj;
    }

    public static List<Category> getTestCase() {
        Category categoryOne = createCategory("Category one", "category-one");
        Category categoryTwo = createCategory("Category two", "category-two");
        Category categoryThree = createCategory("Category three", "category-three");
        return Arrays.asList(categoryOne, categoryTwo, categoryThree);
    }

    private static Category createCategory(String name, String code) {
        Category category = new Category();
        category.name = name;
        category.code = code;
        return category;
    }

    @Override
    public void stop() {
        if (null != wireMockServer) {
            wireMockServer.stop();
        }
    }
}
