package eu.harmonyus.gateway.client.resource;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import eu.harmonyus.gateway.client.model.Tag;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WiremockTagService implements QuarkusTestResourceLifecycleManager {

    private static WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        List<Tag> testCase = getTestCase();
        wireMockServer = new WireMockServer(options().dynamicPort());
        wireMockServer.start();
        WireMock wireMock = new WireMock(wireMockServer.port());
        addMockGetTags(wireMock);
        addMockGetTag(wireMock, "/tags/tag-one", getTag(testCase.get(0), "tag-one"));
        return Collections.singletonMap("harmonyus.microservice.tag/mp-rest/url", wireMockServer.baseUrl());
    }

    public static void enableFaultFeature() {
        wireMockServer.setGlobalFixedDelay(500);
    }

    public static void disableFaultFeature() {
        wireMockServer.setGlobalFixedDelay(0);
    }

    private void addMockGetTag(WireMock wireMock, String url, JSONObject objTag) {
        wireMock.register(get(urlEqualTo(url))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(objTag.toString())));
    }

    private void addMockGetTags(WireMock wireMock) {
        JSONArray objTags = new JSONArray();
        List<Tag> testCase = getTestCase();
        objTags.appendElement(testCase.get(0));
        objTags.appendElement(testCase.get(1));
        objTags.appendElement(testCase.get(2));

        wireMock.register(get(urlEqualTo("/tags"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(objTags.toString())));
    }

    public static JSONObject getTag(Tag tag, String code) {
        JSONObject obj = new JSONObject();
        obj.put("name", tag.name);
        obj.put("code", code);
        return obj;
    }

    public static List<Tag> getTestCase() {
        Tag tagOne = createTag("Tag one", "tag-one");
        Tag tagTwo = createTag("Tag two", "tag-two");
        Tag tagThree = createTag("Tag three", "tag-three");
        return Arrays.asList(tagOne, tagTwo, tagThree);
    }

    private static Tag createTag(String name, String code) {
        Tag tag = new Tag();
        tag.name = name;
        tag.code = code;
        return tag;
    }

    @Override
    public void stop() {
        if (null != wireMockServer) {
            wireMockServer.stop();
        }
    }
}
