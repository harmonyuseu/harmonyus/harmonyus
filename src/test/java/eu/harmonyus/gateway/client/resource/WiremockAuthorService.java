package eu.harmonyus.gateway.client.resource;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import eu.harmonyus.gateway.client.model.Author;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import java.util.*;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WiremockAuthorService implements QuarkusTestResourceLifecycleManager {

    private static WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        List<Author> testCase = getTestCase();
        wireMockServer = new WireMockServer(options().dynamicPort());
        wireMockServer.start();
        WireMock wireMock = new WireMock(wireMockServer.port());
        addMockGetAuthors(wireMock);
        addMockGetAuthor(wireMock, "/authors/john-mathew", getAuthor(testCase.get(0), "john-mathew"));
        return Collections.singletonMap("harmonyus.microservice.author/mp-rest/url", wireMockServer.baseUrl());
    }

    public static void enableFaultFeature() {
        wireMockServer.setGlobalFixedDelay(500);
    }

    public static void disableFaultFeature() {
        wireMockServer.setGlobalFixedDelay(0);
    }

    private void addMockGetAuthor(WireMock wireMock, String url, JSONObject objAuthor) {
        wireMock.register(get(urlEqualTo(url))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(objAuthor.toString())));
    }

    private void addMockGetAuthors(WireMock wireMock) {
        JSONArray objAuthors = new JSONArray();
        List<Author> testCase = getTestCase();
        objAuthors.appendElement(testCase.get(0));
        objAuthors.appendElement(testCase.get(1));
        objAuthors.appendElement(testCase.get(2));

        wireMock.register(get(urlEqualTo("/authors"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(objAuthors.toString())));
    }

    public static JSONObject getAuthor(Author author, String code)  {
        JSONObject obj = new JSONObject();
        obj.put("email", author.email);
        obj.put("firstname", author.firstname);
        obj.put("lastname", author.lastname);
        obj.put("code", code);
        return obj;
    }

    public static List<Author> getTestCase() {
        Author authorJohn = createAuthor("John","Mathew","john.mathew@xyz.com", "john-mathew");
        Author authorJim = createAuthor("Jim","Parker","jim.parker@xyz.com", "jim-parker");
        Author authorSophia = createAuthor("Sophia","Ran","sophia.ran@xyz.com", "sophia-ran");
        return Arrays.asList(authorJohn, authorJim, authorSophia);
    }

    private static Author createAuthor(String firstname, String lastname, String email, String code) {
        Author author = new Author();
        author.firstname = firstname;
        author.lastname = lastname;
        author.email = email;
        author.code = code;
        return author;
    }

    @Override
    public void stop() {
        if (null != wireMockServer) {
            wireMockServer.stop();
        }
    }
}
