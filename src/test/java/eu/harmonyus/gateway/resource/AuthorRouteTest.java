package eu.harmonyus.gateway.resource;

import eu.harmonyus.gateway.client.model.Author;
import eu.harmonyus.gateway.client.resource.WiremockAuthorService;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@QuarkusTestResource(WiremockAuthorService.class)
class AuthorRouteTest {

    @BeforeEach
    void prepare() {
        WiremockAuthorService.disableFaultFeature();
    }

    @Test
    @DisplayName("Test - When Calling GET - /authors should return resources - 200")
    void testGetAll() {
        List<Author> testCase = WiremockAuthorService.getTestCase();

        Author[] authors =  given()
                .when()
                .get("/authors")
                .then()
                .statusCode(200)
                .extract()
                .as(Author[].class);
        assertThat("Authors length", authors.length, equalTo(3));
        assertAuthorValue(authors[0], testCase.get(0), "john-mathew");
        assertAuthorValue(authors[1], testCase.get(1), "jim-parker");
        assertAuthorValue(authors[2], testCase.get(2), "sophia-ran");
    }

    @Test
    @DisplayName("Test - When Calling GET - /authors should return resources - 500")
    void testFaultGetAll() {
        WiremockAuthorService.enableFaultFeature();

        given()
                .when()
                .get("/authors")
                .then()
                .statusCode(500);
    }

    @Test
    @DisplayName("Test - When Calling GET - /authors/{code} should return resources - 200")
    void testGetByCode() {
        List<Author> testCase = WiremockAuthorService.getTestCase();

        Author author =  given()
                .when()
                .get("/authors/john-mathew")
                .then()
                .statusCode(200)
                .extract()
                .as(Author.class);
        assertAuthorValue(author, testCase.get(0), "john-mathew");
    }

    @Test
    @DisplayName("Test - When Calling GET - /authors/{code} should return resources - 500")
    void testFaultGetByCode() {
        WiremockAuthorService.enableFaultFeature();
        given()
                .when()
                .get("/authors/john-mathew")
                .then()
                .statusCode(500);
    }

    private void assertAuthorValue(Author authorActual, Author authorMatcher, String code) {
        assertThat(authorActual.firstname, equalTo(authorMatcher.firstname));
        assertThat(authorActual.lastname, equalTo(authorMatcher.lastname));
        assertThat(authorActual.email, equalTo(authorMatcher.email));
        assertThat(authorActual.code, equalTo(code));
    }
}
