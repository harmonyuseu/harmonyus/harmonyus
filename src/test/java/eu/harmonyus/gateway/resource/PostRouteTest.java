package eu.harmonyus.gateway.resource;

import eu.harmonyus.gateway.client.model.Post;
import eu.harmonyus.gateway.client.resource.WiremockPostService;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@QuarkusTestResource(WiremockPostService.class)
class PostRouteTest {

    @BeforeEach
    void prepare() {
        WiremockPostService.disableFaultFeature();
    }

    @Test
    @DisplayName("Test - When Calling GET - /posts should return resources - 200")
    void testGetAll() {
        List<Post> testCase = WiremockPostService.getTestCase();

        Post[] posts =  given()
                .when()
                .get("/posts")
                .then()
                .statusCode(200)
                .extract()
                .as(Post[].class);
        assertThat("Posts length", posts.length, equalTo(3));
        assertPostValue(posts[2], testCase.get(0), "post-number-one");
        assertPostValue(posts[1], testCase.get(1), "post-number-two");
        assertPostValue(posts[0], testCase.get(2), "post-number-three");
    }

    @Test
    @DisplayName("Test - When Calling GET - /posts should return resources - 500")
    void testFaultGetAll() {
        WiremockPostService.enableFaultFeature();
        given()
                .when()
                .get("/posts")
                .then()
                .statusCode(500);
    }

    @Test
    @DisplayName("Test - When Calling GET - /posts should return resources - 200")
    void testGetByCode() {
        List<Post> testCase = WiremockPostService.getTestCase();

        Post post =  given()
                .when()
                .get("/posts/post-number-one")
                .then()
                .statusCode(200)
                .extract()
                .as(Post.class);
        assertPostValue(post, testCase.get(0), "post-number-one");
    }

    @Test
    @DisplayName("Test - When Calling GET - /posts should return resources - 500")
    void testFaultGetByCode() {
        WiremockPostService.enableFaultFeature();
        given()
                .when()
                .get("/posts/post-number-one")
                .then()
                .statusCode(500);
    }

    private void assertPostValue(Post postActual, Post postMatcher, String code) {
        assertThat(postActual.title, equalTo(postMatcher.title));
        assertThat(postActual.body, equalTo(postMatcher.body));
        assertThat(postActual.author.url, equalTo("http://localhost:8082/authors/" + postMatcher.author.getCode()));
        assertThat(postActual.category.url, equalTo("http://localhost:8082/categories/" + postMatcher.category.getCode()));
        assertThat("Post.tags length", postActual.tags.size(), equalTo(3));
        assertThat(postActual.tags.get(0).url, equalTo("http://localhost:8082/tags/" + postMatcher.tags.get(0).getCode()));
        assertThat(postActual.tags.get(1).url, equalTo("http://localhost:8082/tags/" + postMatcher.tags.get(1).getCode()));
        assertThat(postActual.tags.get(2).url, equalTo("http://localhost:8082/tags/" + postMatcher.tags.get(2).getCode()));
        assertThat(postActual.code, equalTo(code));
    }
}
