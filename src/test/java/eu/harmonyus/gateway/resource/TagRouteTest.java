package eu.harmonyus.gateway.resource;

import eu.harmonyus.gateway.client.model.Tag;
import eu.harmonyus.gateway.client.resource.WiremockTagService;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@QuarkusTestResource(WiremockTagService.class)
class TagRouteTest {

    @BeforeEach
    void prepare() {
        WiremockTagService.disableFaultFeature();
    }

    @Test
    @DisplayName("Test - When Calling GET - /tags should return resources - 200")
    void testGetAll() {
        List<Tag> testCase = WiremockTagService.getTestCase();
        Tag[] tags = given()
                .when()
                .get("/tags")
                .then()
                .statusCode(200)
                .extract()
                .as(Tag[].class);
        assertThat("Tags length", tags.length, equalTo(3));
        assertTagValue(tags[0], testCase.get(0), "tag-one");
        assertTagValue(tags[1], testCase.get(1), "tag-two");
        assertTagValue(tags[2], testCase.get(2), "tag-three");
    }

    @Test
    @DisplayName("Test - When Calling GET - /tags should return resources - 500")
    void testFaultGetAll() {
        WiremockTagService.enableFaultFeature();
        given()
                .when()
                .get("/tags")
                .then()
                .statusCode(500);
    }

    @Test
    @DisplayName("Test - When Calling GET - /tags/{code} should return resources - 200")
    void testGetByCode() {
        List<Tag> testCase = WiremockTagService.getTestCase();
        Tag tag = given()
                .when()
                .get("/tags/tag-one")
                .then()
                .statusCode(200)
                .extract()
                .as(Tag.class);
        assertTagValue(tag, testCase.get(0), "tag-one");
    }

    @Test
    @DisplayName("Test - When Calling GET - /tags/{code} should return resources - 500")
    void testFaultGetByCode() {
        WiremockTagService.enableFaultFeature();
        given()
                .when()
                .get("/tags/tag-one")
                .then()
                .statusCode(500);
    }

    private void assertTagValue(Tag tagActual, Tag tagMatcher, String code) {
        assertThat(tagActual.name, equalTo(tagMatcher.name));
        assertThat(tagActual.code, equalTo(code));
    }
}
