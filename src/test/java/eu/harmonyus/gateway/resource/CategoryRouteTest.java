package eu.harmonyus.gateway.resource;

import eu.harmonyus.gateway.client.model.Category;
import eu.harmonyus.gateway.client.resource.WiremockCategoryService;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@QuarkusTestResource(WiremockCategoryService.class)
class CategoryRouteTest {

    @BeforeEach
    void prepare() {
        WiremockCategoryService.disableFaultFeature();
    }

    @Test
    @DisplayName("Test - When Calling GET - /categories should return resources - 200")
    void testGetAll() {
        List<Category> testCase = WiremockCategoryService.getTestCase();
        Category[] categories =  given()
                .when()
                .get( "/categories")
                .then()
                .statusCode(200)
                .extract()
                .as(Category[].class);
        assertThat("Categories length", categories.length, equalTo(3));
        assertCategoryValue(categories[0], testCase.get(0), "category-one");
        assertCategoryValue(categories[1], testCase.get(1), "category-two");
        assertCategoryValue(categories[2], testCase.get(2), "category-three");
    }

    @Test
    @DisplayName("Test - When Calling GET - /categories should return resources - 500")
    void testFaultGetAll() {
        WiremockCategoryService.enableFaultFeature();
        given()
                .when()
                .get( "/categories")
                .then()
                .statusCode(500);
    }

    @Test
    @DisplayName("Test - When Calling GET - /categories/{code} should return resources - 200")
    void testGetByCode() {
        List<Category> testCase = WiremockCategoryService.getTestCase();
        Category category =  given()
                .when()
                .get( "/categories/category-one")
                .then()
                .statusCode(200)
                .extract()
                .as(Category.class);
        assertCategoryValue(category, testCase.get(0), "category-one");
    }

    @Test
    @DisplayName("Test - When Calling GET - /categories/{code} should return resources - 500")
    void testFaultGetByCode() {
        WiremockCategoryService.enableFaultFeature();
        given()
                .when()
                .get( "/categories/category-one")
                .then()
                .statusCode(500);
    }

    private void assertCategoryValue(Category categoryActual, Category categoryMatcher, String code) {
        assertThat(categoryActual.name, equalTo(categoryMatcher.name));
        assertThat(categoryActual.code, equalTo(code));
    }
}
