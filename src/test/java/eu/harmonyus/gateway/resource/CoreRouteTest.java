package eu.harmonyus.gateway.resource;

import eu.harmonyus.gateway.client.model.ApplicationParameters;
import eu.harmonyus.gateway.client.resource.WiremockCoreService;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@QuarkusTestResource(WiremockCoreService.class)
class CoreRouteTest {

    @BeforeEach
    void prepare() {
        WiremockCoreService.disableFaultFeature();
    }

    @Test
    @DisplayName("Test - When Calling GET - /api/{version}/application-parameters should return resource - 200")
    void testGetApplicationParameter() {
        ApplicationParameters applicationParameters = given()
                .when()
                .get( "/application-parameters")
                .then()
                .statusCode(200)
                .extract()
                .as(ApplicationParameters.class);
        assertThat(applicationParameters.title, equalTo("harmonyus"));
        assertThat(applicationParameters.version, equalTo("0.1.0"));

    }

    @Test
    @DisplayName("Test - When Calling GET - /api/{version}/application-parameters should return resource - 500")
    void testFaultGetApplicationParameter() {
        WiremockCoreService.enableFaultFeature();
        given()
                .when()
                .get( "/application-parameters")
                .then()
                .statusCode(500);
    }
}
