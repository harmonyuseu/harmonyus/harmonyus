## project under development ##

**WARNING: NOT FOR PRODUCTION TARGET**

**GOAL of this project (for the release 1.0.0)**

**Harmonyus** is a free an open source content management system (CMS) oriented cloud.
**Harmonyus** is written in Java with Quarkus serverless and paired with MongoDB database.
**Harmonyus** can work in native with GraalVM on Linux, MacOs or Windows system.

**GOAL of the next release (0.1.0)**

Application works and can be scaled.
It cannot be configured or modified.
